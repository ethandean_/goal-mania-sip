﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InputField : MonoBehaviour
{
    public string goal;
    public GameObject inputField;
    public GameObject textDisplay;

    public void StoreGoal()
    {
        goal = inputField.GetComponent<Text>().text;
        textDisplay.GetComponent<Text>().text = goal;
    }
}
