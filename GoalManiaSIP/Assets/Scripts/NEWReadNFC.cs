﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitsNFCToolkit;
using UnityEngine.UI;
//namespace DigitsNFCToolkit
//{
    public class NEWReadNFC : MonoBehaviour
    {
		public GameObject recievedGoal;
		/// <summary>GameObject name of this class</summary>
		private const string NAME = "NativeNFCManager";

		/// <summary>The singleton instance of NativeNFCManager</summary>
		private static NativeNFCManager instance;

		/// <summary>The NativeNFC instance of current platform</summary>
		public NativeNFC nfc;

		/// <summary>The singleton instance of NativeNFCManager</summary>
		private static NativeNFCManager Instance
		{
			get
			{
				if (instance == null)
				{
					instance = GameObject.FindObjectOfType<NativeNFCManager>();
					if (instance == null)
					{
						GameObject gameObject = new GameObject(NAME);
						instance = gameObject.AddComponent<NativeNFCManager>();
					}
				}

				return instance;
			}
		}

		public static bool IsNFCTagInfoReadSupported()
		{
			NativeNFC nativeNFC = Instance.nfc;
			return nativeNFC.IsNFCTagInfoReadSupported();
		}
		public static bool IsNDEFReadSupported()
		{
			NativeNFC nativeNFC = Instance.nfc;
			return nativeNFC.IsNDEFReadSupported();
		}

	void Enable()
	{

	}

}
//}
