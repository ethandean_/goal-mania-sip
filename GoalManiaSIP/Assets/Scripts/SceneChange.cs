﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChange : MonoBehaviour
{
    public void NFC()
    {
        SceneManager.LoadScene("NFC Scene");
    }
    public void GM()
    {
        SceneManager.LoadScene("GoalMania");
    }
}
